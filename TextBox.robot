*** Setting ***
Library         Selenium2Library
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser

*** Variables ***
${BROWSER}    	  google chrome
${DELEY}    	  2
${LOGIN URL}      https://demoqa.com/


*** Test Cases ***

Select menu Element
    Click Element    //div[@class="card-up"]
    Page Should Contain    Please select an item from left to start practice.
Select element Text Box
    Click Element   //li[@id="item-0"]
    Page Should Contain    Text Box
Fill data
    #Set Selenium Speed  1 seconds
    Input Text   //input[@id="userName"]     Test tool QA
    Input Text   //input[@id="userEmail"]   test@test.qa
    Input Text   //textarea[@id="currentAddress"]   No.2 Siri bangkoknoi bangkok thailand
    Input TExt   //textarea[@id="permanentAddress"]   same as Current Address
    Click Button    //button[@id="submit"]
Check
    Page Should Contain    Name:Test tool QA
    Page Should Contain    Email:test@test.qa
    Page Should Contain    Current Address :No.2 Siri bangkoknoi bangkok thailand
    Page Should Contain    Permananet Address :same as Current Address
    Close Browser

*** Keywords ***
Open Browser To Login Page
    Open Browser   ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
